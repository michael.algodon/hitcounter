﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.IO;

namespace HitCounter
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            #region TO GET THE CORRECT IP ADDRESS IF SERVER IS BEHIND A LOAD BALANCER OR FIREWALL
            var ss = Request.ServerVariables["SERVER_SOFTWARE"];
            if (!string.IsNullOrEmpty(ss) && ss.Contains("Microsoft-IIS")) // doesn't work w/ Cassini
            {
                string SourceIP = String.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"])
                    ? Request.ServerVariables["REMOTE_ADDR"]
                    : Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                Request.ServerVariables.Set("REMOTE_ADDR", SourceIP);
                Request.ServerVariables.Set("REMOTE_HOST", SourceIP);

                string OrigUrl = Request.ServerVariables["HTTP_X_ORIGINAL_URL"];
                if (!string.IsNullOrEmpty(OrigUrl))
                {
                    var url = new Uri(Request.Url, OrigUrl);
                    Request.ServerVariables.Set("SERVER_NAME", url.Host);
                    Request.ServerVariables.Set("SERVER_PORT", url.Port.ToString());
                }
            }

            #endregion

            //GET THE URL ACCESS BY THE USER
            var request = ((System.Web.HttpApplication)sender).Request;

            HitCounter(request);
        }

        public void HitCounter(HttpRequest request)
        {            
            var userIP = GetIp(); //GET THE IP ADDRESS OF THE USER

            userIP = "180.191.158.188"; //HARDCODED FOR LOCALHOST TESTING PURPOSES

            GeoIPInfo geoIPInfo = new GeoIPInfo();
            geoIPInfo.IPAddress = userIP;
            geoIPInfo.PageAccessed = request.RawUrl;
            
            geoIPInfo.Platform = request.Browser.Platform;
            geoIPInfo.PlatformVersion = GetOSVersion(Request.UserAgent);
            geoIPInfo.BrowserType = request.Browser.Type;

            #region get geolocation of IP Address
            try
            {
                using (var reader = new MaxMind.GeoIP2.DatabaseReader(Server.MapPath("~/App_Data/GeoLite2-City/GeoLite2-City.mmdb")))
                {
                    var city = reader.City(userIP);

                    geoIPInfo.Country = city.Country.Name;
                    geoIPInfo.CountryISOCode = city.Country.IsoCode;
                    geoIPInfo.State = city.MostSpecificSubdivision.Name;
                    geoIPInfo.StateISOCode = city.MostSpecificSubdivision.IsoCode;
                    geoIPInfo.City = city.City.Name;
                    geoIPInfo.PostalCode = city.Postal.Code;
                    geoIPInfo.Lon = (double)city.Location.Longitude;
                    geoIPInfo.Lat = (double)city.Location.Latitude;
                }
            }
            catch { /*THROWS AN ERROR WHEN IP ADDRESS IS INVALID */ }
            #endregion

            #region TESTING ONLY -- REPLACE THIS WITH SAVE TO DATABASE
            var path = HttpContext.Current.Request.PhysicalApplicationPath + "\\Hits\\URLLogs.txt";
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(geoIPInfo.ToString());
            }
            #endregion
        }

        public string GetOSVersion(string useragent)
        {
            try
            {
                return useragent.Substring(useragent.IndexOf("(") + 1, useragent.IndexOf(")") - useragent.IndexOf("(")-1);
            }
            catch
            {
                return useragent;
            }
        }


        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            return ip;
        }

        public class GeoIPInfo
        {
            public DateTime LogDate { get; set; }
            public string IPAddress { get; set; }
            public string Country { get; set; }
            public string CountryISOCode { get; set; }
            public string State { get; set; }
            public string StateISOCode { get; set; }
            public string City { get; set; }
            public string PostalCode { get; set; }
            public double Lon { get; set; }
            public double Lat { get; set; }
            public string PageAccessed { get; set; }
            public string Platform { get; set; }
            public string PlatformVersion { get; set; }
            public string BrowserType { get; set; }

            public GeoIPInfo()
            {
                LogDate = DateTime.Now;
            }
            public override string ToString()
            {
                return LogDate.ToString() + "\n"
                        + IPAddress + "\n"
                        + Platform + "\n"
                        + PlatformVersion + "\n"
                        + BrowserType + "\n"
                        + Country + "\n"
                        + CountryISOCode + "\n"
                        + State + "\n"
                        + StateISOCode + "\n"
                        + City + "\n"
                        + PostalCode + "\n"
                        + Lon + "\n"
                        + Lat + "\n"
                        + PageAccessed + "\n";
            }
        }


    }
}
